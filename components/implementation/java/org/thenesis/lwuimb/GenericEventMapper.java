/*
 * MIDPath - Copyright (C) 2006-2008 Guillaume Legris, Mathieu Legris
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation. 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details. 
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA 
 */
package org.thenesis.lwuimb;


import org.thenesis.lwuimb.configuration.Configuration;
import org.thenesis.microbackend.ui.KeyConstants;

import com.sun.lwuit.Display;


public class GenericEventMapper implements EventMapper {
    
    // Defined in Display class
    private static int KEY_GAME_DOWN;
    private static int KEY_GAME_FIRE ;
    private static int KEY_GAME_LEFT;
    private static int KEY_GAME_RIGHT ;
    private static int KEY_GAME_UP;
    private static int KEY_POUND;

    // Internals
    public static int KEY_BACK;
	public static int KEY_CLEAR;
	public static int KEY_SOFT_LEFT;
	public static int KEY_SOFT_RIGHT;
	public static int KEY_SOFT_RIGHT2;
	
	// Edition keys
	private static int KEY_BACKSPACE;
	private static int KEY_DELETE;

	static {
//		KEY_LEFT = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.LEFT", KeyConstants.VK_LEFT);
//		KEY_UP = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.UP", KeyConstants.VK_UP);
//		KEY_RIGHT = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.RIGHT", KeyConstants.VK_RIGHT);
//		KEY_DOWN = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.DOWN", KeyConstants.VK_DOWN);
//		KEY_GAME_A = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.GAME_A", KeyConstants.VK_F4);
//		KEY_GAME_B = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.GAME_B", KeyConstants.VK_F5);
//		KEY_GAME_C = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.GAME_C", KeyConstants.VK_F6);
//		KEY_GAME_D = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.GAME_D", KeyConstants.VK_F7);
//		KEY_FIRE = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.FIRE", KeyConstants.VK_ENTER);
//		KEY_STAR = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.STAR", KeyConstants.VK_ASTERISK);
//		KEY_POUND = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.POUND", KeyConstants.VK_NUMBER_SIGN);
//		KEY_DELETE = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.DELETE", KeyConstants.VK_BACK_SPACE);
//		KEY_END = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.END", KeyConstants.VK_END);
//		KEY_POWER = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.POWER", KeyConstants.VK_F12);
//		KEY_SOFT_BUTTON1 = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.SOFT_BUTTON1", KeyConstants.VK_F1);
//		KEY_SOFT_BUTTON2 = Configuration.getIntProperty("org.thenesis.midpath.ui.keys.SOFT_BUTTON2", KeyConstants.VK_F2);
	    
	    KEY_GAME_DOWN = Configuration.getIntProperty("org.thenesis.lwuimb.keys.GAME_DOWN", KeyConstants.VK_DOWN);
	    KEY_GAME_FIRE = Configuration.getIntProperty("org.thenesis.lwuimb.keys.GAME_FIRE", KeyConstants.VK_ENTER);
	    KEY_GAME_LEFT = Configuration.getIntProperty("org.thenesis.lwuimb.keys.GAME_LEFT", KeyConstants.VK_LEFT);
	    KEY_GAME_RIGHT = Configuration.getIntProperty("org.thenesis.lwuimb.keys.GAME_RIGHT", KeyConstants.VK_RIGHT);
	    KEY_GAME_UP = Configuration.getIntProperty("org.thenesis.lwuimb.keys.GAME_UP", KeyConstants.VK_UP);
	    KEY_POUND = Configuration.getIntProperty("org.thenesis.lwuimb.keys.POUND", KeyConstants.VK_NUMBER_SIGN);
	    KEY_BACK = Configuration.getIntProperty("org.thenesis.lwuimb.keys.BACK", KeyConstants.VK_F4);
	    KEY_CLEAR = Configuration.getIntProperty("org.thenesis.lwuimb.keys.CLEAR", KeyConstants.VK_F5);
	    KEY_SOFT_LEFT = Configuration.getIntProperty("org.thenesis.lwuimb.keys.SOFT_LEFT", KeyConstants.VK_F1);
	    KEY_SOFT_RIGHT = Configuration.getIntProperty("org.thenesis.lwuimb.keys.SOFT_RIGHT", KeyConstants.VK_F2);
	    KEY_SOFT_RIGHT2 = Configuration.getIntProperty("org.thenesis.lwuimb.keys.SOFT_RIGHT2", KeyConstants.VK_F3);
	    KEY_BACKSPACE = Configuration.getIntProperty("org.thenesis.lwuimb.keys.BACKSPACE", KeyConstants.VK_BACK_SPACE);
	    KEY_DELETE = Configuration.getIntProperty("org.thenesis.lwuimb.keys.DELETE", KeyConstants.VK_DELETE);
	    
	}

	public int getGameAction(int keyCode) {

		if (keyCode == KEY_GAME_DOWN)
			return Display.GAME_DOWN;
		else if (keyCode == KEY_GAME_FIRE)
			return Display.GAME_FIRE;
		else if (keyCode == KEY_GAME_LEFT)
			return Display.GAME_LEFT;
		else if (keyCode == KEY_GAME_RIGHT)
			return Display.GAME_RIGHT;
		else if (keyCode == KEY_GAME_UP)
			return Display.GAME_UP;
		else
			return -1;
	    
	}

	public int getKeyCode(int gameAction) {
	    
	    if (gameAction == Display.GAME_DOWN)
            return KEY_GAME_DOWN;
        else if (gameAction == Display.GAME_FIRE)
            return KEY_GAME_FIRE;
        else if (gameAction == Display.GAME_LEFT)
            return KEY_GAME_LEFT;
        else if (gameAction == Display.GAME_RIGHT)
            return KEY_GAME_RIGHT;
        else if (gameAction == Display.GAME_UP)
            return KEY_GAME_UP;
        else
            return -1;
	    
	}
	
	public int getBackKeyCode() {
        return KEY_BACK;
    }

    public int getClearKeyCode() {
        return KEY_CLEAR;
    }

    public int getSoftLeftKeyCode() {
        return KEY_SOFT_LEFT;
    }

    public int getSoftRightKeyCode() {
        return KEY_SOFT_RIGHT;
    }

    public int getBackspaceKeyCode() {
        return KEY_BACKSPACE;
    }
	

	public String getKeyName(int keyCode) {

//		if (keyCode == Canvas.KEY_POUND)
//			return KeyConstants.getName(KEY_POUND);
//		else if (keyCode == Canvas.KEY_STAR)
//			return KeyConstants.getName(KEY_STAR);
//		else if (keyCode == Constants.KEYCODE_SELECT)
//			return KeyConstants.getName(KEY_FIRE);
//		else if (keyCode == Constants.KEYCODE_DOWN)
//			return KeyConstants.getName(KEY_DOWN);
//		else if (keyCode == Constants.KEYCODE_LEFT)
//			return KeyConstants.getName(KEY_LEFT);
//		else if (keyCode == Constants.KEYCODE_RIGHT)
//			return KeyConstants.getName(KEY_RIGHT);
//		else if (keyCode == Constants.KEYCODE_UP)
//			return KeyConstants.getName(KEY_UP);
//		else
//			return KeyConstants.getName(keyCode);
	    
	    return "Unknown";

	}

	public int getSystemKey(int keyCode) {
		if (keyCode == KEY_BACKSPACE)
			return LWUIMBImplementation.SYSTEM_KEY_BACKSPACE;
		else if (keyCode == KEY_DELETE)
			return LWUIMBImplementation.SYSTEM_KEY_DELETE;
		else
			return 0;
	    
	}

	public int mapToInternalEvent(int keyCode, char c) {

//		// Convert key code to an internal code
//		if (keyCode == KEY_DOWN)
//			return Constants.KEYCODE_DOWN;
//		else if (keyCode == KEY_LEFT)
//			return Constants.KEYCODE_LEFT;
//		else if (keyCode == KEY_RIGHT)
//			return Constants.KEYCODE_RIGHT;
//		else if (keyCode == KEY_UP)
//			return Constants.KEYCODE_UP;
//		else if (keyCode == KEY_SOFT_BUTTON1)
//			return EventConstants.SOFT_BUTTON1;
//		else if (keyCode == KEY_SOFT_BUTTON2)
//			return EventConstants.SOFT_BUTTON2;
//		else if (keyCode == KEY_FIRE)
//			return Constants.KEYCODE_SELECT;
//		else if (keyCode == KEY_STAR)
//			return Canvas.KEY_STAR;
//		else if (keyCode == KEY_POUND)
//			return Canvas.KEY_POUND;
//		else if (keyCode == KEY_DELETE)
//			return Constants.KEYCODE_DELETE;
//		else if (keyCode == KEY_END)
//			return Constants.KEYCODE_END;
//		else if (keyCode == KEY_POWER)
//			return Constants.KEYCODE_POWER;
//
//		// Return the visible character
//		if (c != KeyConstants.CHAR_UNDEFINED) {
//			return c;
//		} else {
//			return keyCode;
//		}
	    
	    return keyCode;

	}

}
