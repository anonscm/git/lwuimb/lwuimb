package org.thenesis.lwuimb;

import java.io.IOException;
import java.io.InputStream;

import org.thenesis.lwuimb.configuration.Configuration;
import org.thenesis.lwuimb.configuration.ConfigurationProperties;
import org.thenesis.microbackend.ui.BackendEventListener;
import org.thenesis.microbackend.ui.graphics.VirtualFont;
import org.thenesis.microbackend.ui.graphics.VirtualGraphics;
import org.thenesis.microbackend.ui.graphics.VirtualImage;
import org.thenesis.microbackend.ui.graphics.VirtualToolkit;

import com.sun.lwuit.Component;
import com.sun.lwuit.Display;
import com.sun.lwuit.Form;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.impl.LWUITImplementation;
import com.sun.lwuit.util.Log;

public class LWUIMBImplementation extends LWUITImplementation {

    public static final int SYSTEM_KEY_BACKSPACE = -2;
    public static final int SYSTEM_KEY_DELETE = -3;

    /**
     * On some devices getKeyCode returns numeric values for game actions, this
     * breaks the code since we filter these values. We pick unused negative
     * values for game keys and assign them to game keys for getKeyCode.
     */
    private static int[] portableKeyCodes;
    private static int[] portableKeyCodeValues;
    /**
     * This member holds the left soft key value
     */
    static int[] leftSK = new int[] { -6 };
    /**
     * This member holds the right soft key value
     */
    static int[] rightSK = new int[] { -7 };
    /**
     * This member holds the back command key value
     */
    static int backSK = -11;
    /**
     * This member holds the clear command key value
     */
    static int clearSK = -8;
    static int backspaceSK = -8;

    private int alpha = 255;
    private int[] rgbArr;
    private boolean flushGraphicsBug;

    private VirtualToolkit virtualToolkit;
    private static VirtualFont defaultFont;
    private EventMapper eventMapper = new GenericEventMapper();
    private VirtualBackendEventListener backendEventListener = new VirtualBackendEventListener();
    private char lastKeyChar;
    private int lastKeyPressed;
    
    private EditForm editForm;

    /**
     * This flag indicates if the drawRGB method is able to draw negative x and
     * y In drawRGB method, some devices such as BlackBerry throw exceptions if
     * you try to give negative values to drawRGB method.
     */
    private static boolean drawNegativeOffsetsInRGB = true;

    //@Override
    public int charWidth(Object nativeFont, char ch) {
        return font(nativeFont).charWidth(ch);
    }

    //@Override
    public int charsWidth(Object nativeFont, char[] ch, int offset, int length) {
        return font(nativeFont).charsWidth(ch, offset, length);
    }

    //@Override
    public void clipRect(Object graphics, int x, int y, int width, int height) {
        ((VirtualGraphics) graphics).clipRect(x, y, width, height);
    }

    //@Override
    public Object createFont(int face, int style, int size) {
        return virtualToolkit.createFont(face, style, size);
    }

    //@Override
    public Object createImage(String path) throws IOException {
        InputStream is = getClass().getResourceAsStream(path);
        return createImage(is);
    }

    //@Override
    public Object createImage(InputStream is) throws IOException {
        return virtualToolkit.createImage(is);
    }

    //@Override
    public Object createImage(byte[] bytes, int offset, int len) {
        return virtualToolkit.createImage(bytes, offset, len);
    }
    
    //@Override
    public Object createImage(int[] rgb, int width, int height) {
        return virtualToolkit.createRGBImage(rgb, width, height, true);
    }

    //@Override
    public Object createMutableImage(int width, int height, int fillColor) {
        VirtualImage i = virtualToolkit.createImage(width, height);
        if (fillColor != 0xffffffff) {
            VirtualGraphics g = i.getGraphics();
            g.setColor(fillColor);
            g.fillRect(0, 0, width, height);
        }
        return i;
    }

    //@Override
    public void drawArc(Object graphics, int x, int y, int width, int height, int startAngle, int arcAngle) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.drawArc(x, y, width, height, startAngle, arcAngle);
    }

    //@Override
    public void drawImage(Object graphics, Object img, int x, int y) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.drawImage((VirtualImage) img, x, y, VirtualGraphics.TOP | VirtualGraphics.LEFT);
    }

    //@Override
    public void drawLine(Object graphics, int x1, int y1, int x2, int y2) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.drawLine(x1, y1, x2, y2);
    }

    //@Override
    public void drawRGB(Object graphics, int[] rgbData, int offset, int x, int y, int w, int h, boolean processAlpha) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        int rgbX = x;
        int rgbY = y;

        //if the x or y are positive simply redirect the call to midp Graphics
        if (rgbX >= 0 && rgbY >= 0) {
            nativeGraphics.drawRGB(rgbData, offset, w, rgbX, rgbY, w, h, processAlpha);
            return;
        }

        //first time try to draw with negative indexes
        if (drawNegativeOffsetsInRGB) {
            try {
                nativeGraphics.drawRGB(rgbData, offset, w, rgbX, rgbY, w, h, processAlpha);
                return;
            } catch (RuntimeException e) {
                //if you failed it might be because you tried to paint with negative
                //indexes
                drawNegativeOffsetsInRGB = false;
            }
        }

        //if the translate causes us to paint out of the bounds
        //we will paint only the relevant rows row by row to avoid some devices bugs
        //such as BB that fails to paint if the coordinates are negative.
        if (rgbX < 0 && rgbX + w > 0) {
            if (w < rgbData.length) {
                for (int i = 1; i <= rgbData.length / w; i++) {
                    offset = -rgbX + (w * (i - 1));
                    rgbY++;
                    if (rgbY >= 0) {
                        nativeGraphics.drawRGB(rgbData, offset, (w + rgbX), 0, rgbY, w + rgbX, 1, processAlpha);
                    }
                }
            }
        }
    }

    //@Override
    public void drawRect(Object graphics, int x, int y, int width, int height) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.drawRect(x, y, width, height);
    }

    //@Override
    public void drawRoundRect(Object graphics, int x, int y, int width, int height, int arcWidth, int arcHeight) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
    }

    //@Override
    public void drawString(Object graphics, String str, int x, int y) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.drawString(str, x, y, VirtualGraphics.TOP | VirtualGraphics.LEFT);
    }

    //@Override
    public void editString(Component cmp, int maxSize, int constraint, String text) {
        if (Log.TRACE_ENABLED)
            System.out.println("[DEBUG] VirtualImplementation.editString()");

        EditForm editForm = new EditForm(this, Display.getInstance().getCurrent(), (TextArea) cmp);
        editForm.show();
    }

    //@Override
    public void fillArc(Object graphics, int x, int y, int width, int height, int startAngle, int arcAngle) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.fillArc(x, y, width, height, startAngle, arcAngle);
    }

    //@Override
    public void fillRect(Object graphics, int x, int y, int w, int h) {

        if (isAlphaGlobal()) {
            VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
            nativeGraphics.fillRect(x, y, w, h);
            return;
        }

        if (alpha == 0) {
            return;
        }

        if (alpha == 0xff) {
            VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
            nativeGraphics.fillRect(x, y, w, h);
        } else {
            int transparencyLevel = alpha << 24;
            int color = (getColor(graphics) & 0x00FFFFFF);
            color = (color | transparencyLevel);

            if (rgbArr == null || rgbArr.length < w) {
                rgbArr = new int[w];
            }
            for (int i = 0; i < w; i++) {
                rgbArr[i] = color;
            }

            int rgbX = x;
            int rgbY = y;

            if (rgbX < 0 && rgbX + w > 0) {
                w = rgbX + w;
                rgbX = 0;
            }

            if (w < 0) {
                return;
            }

            for (int i = 0; i < h; i++) {
                if (rgbX >= 0 && rgbY + i >= 0) {
                    drawRGB(graphics, rgbArr, 0, rgbX, rgbY + i, w, 1, true);
                }
            }

        }

    }

    //@Override
    public void fillRoundRect(Object graphics, int x, int y, int width, int height, int arcWidth, int arcHeight) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.fillRoundRect(x, y, width, height, arcWidth, arcHeight);
    }

    //@Override
    public void flushGraphics(int x, int y, int width, int height) {

        if (Log.TRACE_ENABLED)
            System.out.println("[DEBUG] VirtualImplementation.flushGraphics(x, y, w, h)");

        // disable flush graphics bug when media is playing to prevent the double buffer
        // from clearing the media and producing flickering
        Form current = getCurrentForm();
        if (!flushGraphicsBug || (current != null && current.hasMedia())) {
            virtualToolkit.flushGraphics(x, y, width, height);
        } else {
            flushGraphics();
        }

    }

    //@Override
    public void flushGraphics() {
        if (Log.TRACE_ENABLED)
            System.out.println("[DEBUG] VirtualImplementation.flushGraphics()");
        virtualToolkit.flushGraphics(0, 0, getDisplayWidth(), getDisplayHeight());
    }

    //@Override
    public int getAlpha(Object graphics) {
        return alpha;
    }

    //    /**
    //     * This member holds the left soft key value
    //     */
    //    static int[] leftSK = new int[] {-6};
    //    /**
    //     * This member holds the right soft key value
    //     */
    //    static int[] rightSK = new int[]{-7};
    //    /**
    //     * This member holds the back command key value
    //     */
    //    static int backSK = -11;
    //    /**
    //     * This member holds the clear command key value
    //     */
    //    static int clearSK = -8;
    //    static int backspaceSK = -8;

    private void setSoftKeyCodes() {
        // Initialy set known key codes
        leftSK[0] = eventMapper.getSoftLeftKeyCode();
        rightSK[0] = eventMapper.getSoftRightKeyCode();
        backspaceSK = eventMapper.getBackspaceKeyCode();
        clearSK = eventMapper.getClearKeyCode();
        backSK = eventMapper.getBackKeyCode();

        try {
            // if the back key is assigned to a game action by mistake then
            // workaround it implicitly
            int game = getGameAction(backSK);
            if (game == Display.GAME_UP || game == Display.GAME_DOWN || game == Display.GAME_RIGHT || game == Display.GAME_LEFT
                    || game == Display.GAME_FIRE) {
                backSK = -50000;
            }
        } catch (Exception ok) {
        }

        try {
            // if the clear key is assigned to a game action by mistake then
            // workaround it implicitly
            int game = getGameAction(clearSK);
            if (game == Display.GAME_UP || game == Display.GAME_DOWN || game == Display.GAME_RIGHT || game == Display.GAME_LEFT
                    || game == Display.GAME_FIRE) {
                clearSK = -50000;
            }
            game = getGameAction(backspaceSK);
            if (game == Display.GAME_UP || game == Display.GAME_DOWN || game == Display.GAME_RIGHT || game == Display.GAME_LEFT
                    || game == Display.GAME_FIRE) {
                backspaceSK = -50000;
            }
        } catch (Exception ok) {
        }

    }

    //@Override
    public int getBackKeyCode() {
        return backSK;
    }

    //@Override
    public int getBackspaceKeyCode() {
        return backspaceSK;
    }

    //@Override
    public int getClearKeyCode() {
        return clearSK;
    }

    //@Override
    public int getClipHeight(Object graphics) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        return nativeGraphics.getClipHeight();
    }

    //@Override
    public int getClipWidth(Object graphics) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        return nativeGraphics.getClipWidth();
    }

    //@Override
    public int getClipX(Object graphics) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        return nativeGraphics.getClipX();
    }

    //@Override
    public int getClipY(Object graphics) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        return nativeGraphics.getClipY();
    }

    //@Override
    public int getColor(Object graphics) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        return nativeGraphics.getColor();
    }

    //@Override
    public Object getDefaultFont() {
        return virtualToolkit.getDefaultFont();
    }
    
    //@Override
    public int getDisplayWidth() {
        return virtualToolkit.getWidth() - 1;
    }

    //@Override
    public int getDisplayHeight() {
        return virtualToolkit.getHeight() - 1;
    }

    //@Override
    public int getGameAction(int keyCode) {
        try {
            // prevent game actions from being returned by numeric keypad thus screwing up
            // keypad based navigation and text input
            if (keyCode >= '0' && keyCode <= '9') {
                return 0;
            }
            if (portableKeyCodes != null) {
                for (int iter = 0; iter < portableKeyCodeValues.length; iter++) {
                    if (portableKeyCodeValues[iter] == keyCode) {
                        return portableKeyCodes[iter];
                    }
                }
            }

            return eventMapper.getGameAction(keyCode);
        } catch (IllegalArgumentException err) {
            // this is a stupid MIDP requirement some implementations throw this
            // exception for some keys
            return 0;
        }
    }

    //@Override
    public int getHeight(Object nativeFont) {
        return font(nativeFont).getHeight();
    }

    //@Override
    public int getImageHeight(Object i) {
        return ((VirtualImage) i).getHeight();
    }

    //@Override
    public int getImageWidth(Object i) {
        return ((VirtualImage) i).getWidth();
    }

    //@Override
    public int getKeyCode(int gameAction) {
        if (portableKeyCodes == null) {
            portableKeyCodes = new int[] { Display.GAME_DOWN, Display.GAME_LEFT, Display.GAME_RIGHT, Display.GAME_UP, Display.GAME_FIRE };
            portableKeyCodeValues = new int[5];
            int currentValue = -500;
            int offset = 0;
            while (offset < portableKeyCodeValues.length) {
                currentValue--;
                try {
                    if (eventMapper.getGameAction(currentValue) != 0) {
                        continue;
                    }
                } catch (IllegalArgumentException ignor) {
                    // this is good, the game key is unassigned
                }
                portableKeyCodeValues[offset] = currentValue;
                offset++;
            }
        }
        for (int iter = 0; iter < portableKeyCodes.length; iter++) {
            if (portableKeyCodes[iter] == gameAction) {
                return portableKeyCodeValues[iter];
            }
        }
        return 0;
    }

    //@Override
    public Object getNativeGraphics() {
        if (Log.TRACE_ENABLED)
            System.out.println("[DEBUG] VirtualImplementation.getGraphics()");
        return virtualToolkit.getRootGraphics();
    }

    //@Override
    public Object getNativeGraphics(Object image) {
        return ((VirtualImage) image).getGraphics();
    }

    //@Override
    public void getRGB(Object nativeImage, int[] arr, int offset, int x, int y, int width, int height) {
        ((VirtualImage) nativeImage).getRGB(arr, offset, width, x, y, width, height);
    }

    //@Override
    public int[] getSoftkeyCode(int index) {
        if (index == 0) {
            return leftSK;
        }
        if (index == 1) {
            return rightSK;
        }
        return null;
    }

    //@Override
    public int getSoftkeyCount() {
        return 2;
    }

    //@Override
    public void init(Object m) {
        //Copy LWUIT config in the MicroBackend config
        ConfigurationProperties properties = Configuration.getAllProperties();
        org.thenesis.microbackend.ui.Configuration microBackendConfig = new org.thenesis.microbackend.ui.Configuration();
        for (int i = 0; i < properties.size(); i++) {
            microBackendConfig.addParameter(properties.getKeyAt(i), properties.getValueAt(i));
        }
       
        virtualToolkit = VirtualToolkit.createToolkit(microBackendConfig, backendEventListener);
        virtualToolkit.initialize(m);

        setSoftKeyCodes();
    }

    //@Override
    public boolean isTouchDevice() {
        // TODO 
        return false;
    }

    //@Override
    public Object scale(Object nativeImage, int width, int height) {
        VirtualImage image = (VirtualImage) nativeImage;
        int srcWidth = image.getWidth();
        int srcHeight = image.getHeight();

        // no need to scale
        if (srcWidth == width && srcHeight == height) {
            return image;
        }

        int[] currentArray = new int[srcWidth];
        int[] destinationArray = new int[width * height];
        scaleArray(image, srcWidth, srcHeight, height, width, currentArray, destinationArray);

        return createImage(destinationArray, width, height);
    }

    private void scaleArray(VirtualImage currentImage, int srcWidth, int srcHeight, int height, int width, int[] currentArray,
            int[] destinationArray) {
        // Horizontal Resize
        int yRatio = (srcHeight << 16) / height;
        int xRatio = (srcWidth << 16) / width;
        int xPos = xRatio / 2;
        int yPos = yRatio / 2;

        // if there is more than 16bit color there is no point in using mutable
        // images since they won't save any memory
        for (int y = 0; y < height; y++) {
            int srcY = yPos >> 16;
            getRGB(currentImage, currentArray, 0, 0, srcY, srcWidth, 1);
            for (int x = 0; x < width; x++) {
                int srcX = xPos >> 16;
                int destPixel = x + y * width;
                if ((destPixel >= 0 && destPixel < destinationArray.length) && (srcX < currentArray.length)) {
                    destinationArray[destPixel] = currentArray[srcX];
                }
                xPos += xRatio;
            }
            yPos += yRatio;
            xPos = xRatio / 2;
        }
    }

    //@Override
    public void setAlpha(Object graphics, int alpha) {
        this.alpha = alpha;
    }

    //@Override
    public void setClip(Object graphics, int x, int y, int width, int height) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.setClip(x, y, width, height);
    }

    //@Override
    public void setColor(Object graphics, int RGB) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.setColor(RGB);
    }

    //@Override
    public void setNativeFont(Object graphics, Object font) {
        VirtualGraphics nativeGraphics = (VirtualGraphics) graphics;
        nativeGraphics.setFont(font(font));
    }

    //@Override
    public int stringWidth(Object nativeFont, String str) {
        return font(nativeFont).stringWidth(str);
    }

    //@Override
    public int getFace(Object nativeFont) {
        return font(nativeFont).getFace();
    }
    
    //@Override
    public int getSize(Object nativeFont) {
        return font(nativeFont).getSize();
    }
    
    //@Override
    public int getStyle(Object nativeFont) {
        return font(nativeFont).getStyle();
    }
    
    /**
     * @inheritDoc
     */
    public void saveTextEditingState() {
        if (Log.TRACE_ENABLED)
            System.out.println("[DEBUG] LWUIMBImplementation.saveTextEditingState()");
        
        // FIXME ?
//        String text = editForm.getString();
//        Display.getInstance().onEditingComplete(currentTextComponent, text);
//        currentTextBox = null;
//        waitForEdit.setDone(true);
    }

    private VirtualFont font(Object f) {
        if (f == null) {
            return (VirtualFont) getDefaultFont();
        }
        return (VirtualFont) f;
    }

    EventMapper getEventMapper() {
        return eventMapper;
    }

    char getLastKeyChar() {
        return lastKeyChar;
    }

    /**
     * Listens events coming from the UIBackend and send it to the event queue
     */
    private class VirtualBackendEventListener implements BackendEventListener {

        private boolean dragEnabled = false;

        public VirtualBackendEventListener() {
        }

        public void keyPressed(int keyCode, char c, int modifiers) {

            if (Log.TRACE_ENABLED)
                System.out.println("[DEBUG] VirtualBackendEventListener.keyPressed(): keyCode=" + keyCode + " c=" + c);

            LWUIMBImplementation.this.keyPressed(keyCode);

            lastKeyPressed = keyCode;
            lastKeyChar = c;
        }

        public void keyReleased(int keyCode, char c, int modifiers) {

            if (Log.TRACE_ENABLED)
                System.out.println("[DEBUG] VirtualBackendEventListener.keyReleased(): keyCode=" + keyCode + " c=" + c);

            LWUIMBImplementation.this.keyReleased(keyCode);
        }

        public void pointerMoved(int x, int y, int modifiers) {
            if (Log.TRACE_ENABLED)
                System.out.println("[DEBUG] VirtualBackendEventListener.mouseMoved(): x=" + x + " y=" + y + " drag enabled ? "
                        + dragEnabled);

            if (dragEnabled) {
                LWUIMBImplementation.this.pointerDragged(x, y);
            } else {
                LWUIMBImplementation.this.pointerHover(x, y);
            }

        }

        public void pointerPressed(int x, int y, int modifiers) {
            if (Log.TRACE_ENABLED)
                System.out.println("[DEBUG] VirtualBackendEventListener.mousePressed(): x=" + x + " y=" + y);

            dragEnabled = true;
            LWUIMBImplementation.this.pointerPressed(x, y);
        }

        public void pointerReleased(int x, int y, int modifiers) {
            if (Log.TRACE_ENABLED)
                System.out.println("[DEBUG] VirtualBackendEventListener.mouseReleased(): x=" + x + " y=" + y);
            dragEnabled = false;
            LWUIMBImplementation.this.pointerReleased(x, y);
        }

        public void windowClosed() {
            if (Log.TRACE_ENABLED)
                System.out.println("[DEBUG] VirtualBackendEventListener.windowClosed(): Window delete event received");

        }

    }

}
