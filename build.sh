#!/bin/bash
# Usage: type ./build.sh --help

# Default commands and library locations
JAVAC_CMD=javac
JAR_CMD=jar
JAR_FLAGS=cvf
FASTJAR_ENABLED=no

DIST_HOME=`pwd`
JAR_DIST_HOME=$DIST_HOME/dist

# The file to use if you do not built it
# yourself or the target name if it is
# build
CLDC_JAR=${DIST_HOME}/lib/cldc1.1.jar
J2SE_JAR=/usr/share/classpath/glibj.zip
MICROBACKEND_JAR=${DIST_HOME}/lib/microbackend.jar

CLDC_FLAGS="-source 1.3 -target 1.1"

# Defaults
CORE_ENABLED=yes
DEMO_ENABLED=yes

# Overridable file names and default locations
CORE_JAR=${JAR_DIST_HOME}/lwuimb-0.1-alpha1.jar
DEMO_JAR=${JAR_DIST_HOME}/lwuimb-demo.jar
GLUE_JAR=${DIST_HOME}/lib/glue.jar

#==========================================
# You should not change anything below
#==========================================

OPTIONS="\
help,\
\
disable-core,\
disable-demo,\
\
with-cldc-jar:,\
with-j2se-jar:,\
with-microbackend-jar:,\
\
with-jar:,\
with-javac:\
"

TEMP=`getopt -l $OPTIONS -o h -- "$@"`

eval set -- "$TEMP"

while true; do
  case $1 in
    --help ) echo "Usage : $(basename $0) [options...]"
      echo "  --help                    : Show this help"
      echo
      echo "Core features:"
      echo "  --disable-core  : Do not compile core library (default: yes)"
      echo "  --disable-demo  : Do not compile demo (default: yes)"
      echo
      echo "Providable libraries:"
      echo "  --with-cldc-jar           : Location of the CLDC class library"
      echo "  --with-j2se-jar           : Location of the J2SE class library"
      echo "  --with-microbackend-jar   : Location of the MicroBackend library"
      echo
      echo "External programs:"
      echo "  --with-jar                : Location and name of the jar tool (default: $JAR_CMD)"
      echo "  --with-javac              : Location and name of the javac tool (default: $JAVAC_CMD)"
      echo
      exit 0
      ;;
    --disable-core ) CORE_ENABLED=no
      echo "Compiling core library disabled"
      shift ;;
    --disable-demo ) DEMO_ENABLED=no
      echo "Compiling demos disabled"
      shift ;;
    --with-cldc-jar )
      CLDC_JAR=$2
      echo "Using CLDC class library at: $CLDC_JAR"
      shift 2 ;;
    --with-j2se-jar )
      J2SE_JAR=$2
      echo "using J2SE class library at: $J2SE_JAR"
      shift 2 ;;
    --with-microbackend-jar )
      MICROBACKEND_JAR=$2
      echo "Using MicroBackend library at: $MICROBACKEND_JAR"
      shift 2 ;;
    --with-jar )
      JAR_CMD=$2
      echo "Using jar command: $JAR_CMD"
      shift 2 ;;
    --with-javac )
      JAVAC_CMD=$2
      echo "Using javac command: $JAVAC_CMD"
      shift 2 ;;
    -- ) shift; break;;
    * ) echo "Unknown argument: $1"; break ;;
  esac
done

# Create the dist directory
if [ ! -d $JAR_DIST_HOME ]; then
  mkdir $JAR_DIST_HOME
fi

# Builds mmake-managed Java sources and creates a Jar.
#
# $1 - yes/no = whether the build should be done or not
# $2 - source directory
# $3 - target jar file name and location (must be absolute!)
# $4 - auxiliary bootclasspath entries (optional)
#      containing a leading colon (:) character
build_java ()
{
  if [ $1 = yes ]
  then
    local srcdir=$2
    local jarname=$3
    local auxbcp=$4

    make -C $srcdir \
      JAVAC=$JAVAC_CMD \
      JAVAC_FLAGS="-bootclasspath ${CLDC_JAR}$auxbcp -sourcepath . $CLDC_FLAGS" || exit 1

    make jar -C $srcdir \
      JAVAC=$JAVAC_CMD \
      JAVAC_FLAGS="-bootclasspath ${CLDC_JAR}$auxbcp -sourcepath . $CLDC_FLAGS" \
      JAR=$JAR_CMD \
      JAR_FLAGS="$JAR_FLAGS" \
      JAR_FILE="$jarname" || exit 1
  else
    echo "skipping: $2"
  fi
}

# Builds mmake-managed Java sources, creates a Jar and adds resources.
#
# $1 - yes/no = whether the build should be done or not
# $2 - source directory
# $3 - resource directory
# $4 - target jar file name and location (must be absolute!)
# $5 - auxilliary bootclasspath entries (optional)
#      containing a leading colon (:) character
build_java_res()
{
  build_java $1 $2 $4 $5 || exit 1

  if [ $1 = yes ]; then
    local resdir=$3
    local jarname=$4
    
    if [ $FASTJAR_ENABLED = yes ]; then
      # fastjar needs to get the file list via stdin
      ( cd $resdir && find -type f | grep -v "/.svn" | $JAR_CMD uvf $jarname -E -M -@ )
    else
      # Sun's jar has trouble with the first entry when using @ and -C
      echo "ignore_the_error" > resources.list
      # all other jar commands handle the resources via a file
      find $resdir -type f | grep -v "/.svn" >> resources.list
      $JAR_CMD uvf $jarname -C $resdir @resources.list
    fi
  fi
}

#------------------------
# Build jars
#------------------------

# Build glue code
build_java yes components/glue/java $GLUE_JAR

# Sync with the official LWUIT directory
USERNAME=java-net-username
svn export https://lwuit.dev.java.net/svn/lwuit/trunk/MIDP/LWUIT/src lwuit --username $USERNAME
rm -rf lwuit/com/sun/lwuit/M3G.java
rm -rf lwuit/com/sun/lwuit/impl/ImplementationFactory.java
rm -rf lwuit/com/sun/lwuit/util/Log.java
rm -rf lwuit/com/sun/lwuit/impl/midp
rm -rf lwuit/com/sun/lwuit/SVGImage.java
cp -rf lwuit/* components/core/java/ 
rm -rf lwuit

# Add the overlay
cp -rf components/overlay/java/* components/core/java/

# Build core library
build_java $CORE_ENABLED components/core/java $CORE_JAR :$MICROBACKEND_JAR:$GLUE_JAR

# Build demo  
build_java_res $DEMO_ENABLED demos/java demos/resources $DEMO_JAR :$J2SE_JAR:$CORE_JAR

# Copy microbackend.jar to the dist directory
cp $DIST_HOME/lib/microbackend.jar $JAR_DIST_HOME


