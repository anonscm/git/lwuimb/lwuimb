package org.thenesis.lwuimb.demo;
import java.io.IOException;
import java.util.Hashtable;

import com.sun.lwuit.Button;
import com.sun.lwuit.ButtonGroup;
import com.sun.lwuit.CheckBox;
import com.sun.lwuit.ComboBox;
import com.sun.lwuit.Command;
import com.sun.lwuit.Component;
import com.sun.lwuit.Container;
import com.sun.lwuit.Display;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.RadioButton;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BorderLayout;
import com.sun.lwuit.layouts.BoxLayout;
import com.sun.lwuit.plaf.UIManager;

public class BaseDemo {
    
    private Command backCommand = new Command("Back to the menu");
    private Command exitCommand = new Command("Exit");
    
    public static void main(String[] args) {
        BaseDemo demo = new BaseDemo();
        demo.start();
    }
    
    public void start() {
        init();
        setupTheme();
        Form mainForm = buildUI();
        mainForm.show();
    }
    
    
    // Must be overridden by subclasses 
    public void init() {
        Display.init(null);
    }
    
    // Must be overridden by subclasses 
    public void exit() {
        System.exit(0);
    }
    
    public Form buildUI() {
        
        final Form mainForm = new Form("Main Menu");
        mainForm.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        
        // Create buttons in the main form
        final Button buttonDemoButton = new Button("Button Demo");
        final Button listDemoButton = new Button("List Demo");
        final Button textAreaDemoButton = new Button("TextArea Demo");
        final Button layoutDemoButton = new Button("Layout Demo");
        final Button transitionDemoButton = new Button("Transition Demo");
        
        // Create demo forms
        final Form buttonForm = createButtonForm();
        final Form listForm = createListForm();
        final Form textAreaForm = createTextAreaForm();
        final Form layoutForm = createLayoutForm();
        final Form transitionForm = createTransitionForm();
        
        // Add an action controler
        ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == backCommand) {
                    mainForm.show();
                    return;
                } else if (evt.getCommand() == exitCommand) {
                    exit();
                }
                
                if (evt.getSource() == buttonDemoButton) {
                    buttonForm.show();
                } else if (evt.getSource() == listDemoButton) {
                    listForm.show();
                } else if (evt.getSource() == textAreaDemoButton) {
                    textAreaForm.show();
                } else if (evt.getSource() == layoutDemoButton) {
                    layoutForm.show();
                } else if (evt.getSource() == transitionDemoButton) {
                    transitionForm.show();
                }
            }
        };
        
        // Add action listener to the buttons and forms
        buttonDemoButton.addActionListener(listener);
        listDemoButton.addActionListener(listener);
        textAreaDemoButton.addActionListener(listener);
        layoutDemoButton.addActionListener(listener);
        transitionDemoButton.addActionListener(listener);
        
        buttonForm.addCommand(backCommand);
        buttonForm.setCommandListener(listener);
        listForm.addCommand(backCommand);
        listForm.setCommandListener(listener);
        textAreaForm.addCommand(backCommand);
        textAreaForm.setCommandListener(listener);
        layoutForm.addCommand(backCommand);
        layoutForm.setCommandListener(listener);
        transitionForm.addCommand(backCommand);
        transitionForm.setCommandListener(listener);
        
        mainForm.addComponent(buttonDemoButton);
        mainForm.addComponent(listDemoButton);
        mainForm.addComponent(textAreaDemoButton);
        mainForm.addComponent(layoutDemoButton);
        mainForm.addComponent(transitionDemoButton);
        mainForm.addCommand(exitCommand);
        mainForm.setCommandListener(listener);
        
        return mainForm;
       
    }
    
    public void setupTheme() {
     // Theme
        Hashtable themeMap = new Hashtable();
        themeMap.put("bgColor", "000000");
        themeMap.put("fgColor", "FFFFFF");
        
        themeMap.put("Form.bgImage", "/res/cosmic.png");
        themeMap.put("Button.bgColor", "000000");
        themeMap.put("Button.fgColor", "FFFFFF");
        themeMap.put("Button.bgImage", "/res/softbutton.png");
        //themeMap.put("Label.bgImage", "/res/cosmic.png");
        themeMap.put("Label.bgColor", "113663");
        themeMap.put("Label.fgColor", "FFFFFF");
        themeMap.put("Label.bgSelColor", "113663");
        themeMap.put("Label.fgSelColor", "FFFFFF");
        //themeMap.put("Label.bgImage", "/res/cosmic.png");
        themeMap.put("RadioButton.bgImage", "/res/bar.png");
        themeMap.put("RadioButton.bgColor", "000000");
        themeMap.put("RadioButton.fgColor", "FFFFFF");
        themeMap.put("RadioButton.bgSelColor", "000000");
        themeMap.put("RadioButton.fgSelColor", "FFFFFF");
        themeMap.put("TextArea.bgColor", "000000");
        themeMap.put("TextArea.fgColor", "FFFFFF");
        themeMap.put("TextArea.bgImage", "/res/softbutton.png");
        themeMap.put("List.bgImage", "/res/softbutton.png");
        themeMap.put("ComboBox.bgImage", "/res/softbutton.png");
        themeMap.put("Title.bgImage", "/res/softbutton.png");
        themeMap.put("SoftButton.bgImage", "/res/softbutton.png");
//        themeMap.put("SoftButton.bgColor", "333333");
//        themeMap.put("SoftButton.fgColor", "FFFFFF");
        UIManager.getInstance().setThemeProps(themeMap);
    }

    /* Filling forms demos */

    public Form createButtonForm() {


        Form f = new Form("Button Demo");
        f.setLayout(new BoxLayout(BoxLayout.Y_AXIS));

        RadioButton rb;
        ButtonGroup group = new ButtonGroup();

        rb = new RadioButton("Grilled chicken");
        group.add(rb);
        f.addComponent(rb);

        rb = new RadioButton("Filet mignon");
        group.add(rb);
        f.addComponent(rb);

        rb = new RadioButton("Mahi mahi");
        group.add(rb);
        f.addComponent(rb);

        rb = new RadioButton("Chili");
        group.add(rb);
        f.addComponent(rb);

        CheckBox cb;

        cb = new CheckBox("Guacamole");
        f.addComponent(cb);

        cb = new CheckBox("Tabasco sauce");
        f.addComponent(cb);

        cb = new CheckBox("Mango salsa");
        f.addComponent(cb);

        cb = new CheckBox("Mayonnaise");
        f.addComponent(cb);

        cb = new CheckBox("Whipped cream");
        f.addComponent(cb);

        return f;

    }

    public Form createListForm() {

        Form f = new Form("List Demo");
        List list = new List();
        list.addItem("uno");
        list.addItem("due");
        list.addItem("tre");
        list.addItem("quattro");
        list.addItem("cinque");
        list.addItem("sei");
        f.addComponent(list);

        ComboBox comboBox = new ComboBox(list.getModel());
        f.addComponent(comboBox);

        return f;
    }

    public Form createTextAreaForm() {

        Form f = new Form("TextArea Demo");
//        TextArea area = new TextArea("Peppino");
//        f.addComponent(area);

        String s = "You can edit this text area.\n\n";
        s += "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, "
                + "sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat."
                + "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. "
                + "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat "
                + "nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.";
        TextArea big = new TextArea(s, 5, 35);
        f.addComponent(big);

        return f;
    }

    public Form createLayoutForm() {

        Form f = new Form("Layout Demo");
        f.setLayout(new BorderLayout());

        Image image;
        try {
            image = Image.createImage("/res/baldy.png");
            Label bottomText = new Label(image);
            bottomText.setAlignment(Component.CENTER);
            bottomText.setText("Baldassare");
            bottomText.setTextPosition(Component.BOTTOM);

            Container buttonBar = new Container(new BoxLayout(BoxLayout.X_AXIS));
            buttonBar.addComponent(new Button("Add"));
            buttonBar.addComponent(new Button("Remove"));
            buttonBar.addComponent(new Button("Edit"));
            buttonBar.addComponent(new Button("Send"));
            buttonBar.addComponent(new Button("Exit"));

            f.addComponent(BorderLayout.CENTER, bottomText);
            f.addComponent(BorderLayout.SOUTH, buttonBar);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return f;

    }

    /* Transition tests */

    public Form createTransitionForm() {

        final Form mFirstForm = new Form("First Form");

        Button button = new Button("Switch");
        mFirstForm.addComponent(button);
        mFirstForm.setTransitionOutAnimator(CommonTransitions.createSlide(CommonTransitions.SLIDE_HORIZONTAL, false, 200));

        final Form mSecondForm = new Form("Second Form");

        Button button2 = new Button("Switch back to the first form");
        mSecondForm.addComponent(button2);
        mSecondForm.setTransitionOutAnimator(CommonTransitions.createSlide(CommonTransitions.SLIDE_HORIZONTAL, true, 200));

        ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                Form current = Display.getInstance().getCurrent();
                if (current == mFirstForm) {
                    mSecondForm.show();
                } else if (current == mSecondForm) {
                    mFirstForm.show();
                }
            }
        };
        button.addActionListener(listener);
        button2.addActionListener(listener);

        return mFirstForm;

    }

    

}
