package org.thenesis.lwuimb.demo;

import java.awt.Frame;

import com.sun.lwuit.Display;

public class ExplicitModeDemo extends BaseDemo {
    
    Frame frame;
    
    public static void main(String[] args) {
        ExplicitModeDemo demo = new ExplicitModeDemo();
        demo.start();
    }
    
    // Overrides parent method 
    public void init() {
        frame = new Frame();
        frame.setSize(320, 240);
        Display.init(frame);
    }
    
    // Overrides parent method 
    public void start() {
        super.start();
        frame.pack();
        frame.setVisible(true);
    }
    
    // Overrides parent method 
    public void exit() {
        System.exit(0);
    }

}
